CC=nasm
CFLAGS=-f elf64

all: main


dict.o: dict.asm
	$(CC) $(CFLAGS) dict.asm -o dict.o

lib.o: lib.asm
	$(CC) $(CFLAGS) lib.asm -o lib.o
main.o: main.asm colon.inc words.inc
	$(CC) $(CFLAGS) main.asm -o main.o
	
main: main.o dict.o lib.o
	ld main.o lib.o dict.o -o main

test: main
	./test.py

clean:
	rm -f *.o main

.PHONY: all clean test
