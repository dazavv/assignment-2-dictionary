%include "lib.inc"
%include "colon.inc"
%include "words.inc"
%include "dict.inc

%define BUFFER_SIZE 256

section .rodata
error_message: db "Word not found", 0
incorrect_input_message: db "Incorrect input", 0

section .bss
buffer: resb BUFFER_SIZE

section .text
global _start:

_start:
	mov rdi, buffer
	mov rsi, 255
	call read_word
	test rax, rax
	jz .incorrect_input

	mov rdi, buffer
	mov rsi, NEXT
	call find_word
	test rax, rax
	jz .not_found
		
	lea rdi, [rax + 8 + rdx + 1] ; Теперь rdi хранит указатель на значение узла связанного списка
	call print_string
	jmp .end

	.not_found:
		mov rdi, not_found_message
		call print_error
		jmp .end
	.incorrect_input:
		mov rdi, incorrect_input_message
		call print_error
	.end:
		call exit
