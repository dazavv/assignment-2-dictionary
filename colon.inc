%define NEXT 0
%macro colon 2 	; принимает 2 параметра(строку, указатель)
    %ifstr %1
        db %1, 0
    %else
        %error "Invalid first value"
    %endif 
    
    %ifid %2
        %2: 
            dq NEXT 		; хранит указатель на следующую строку
            %define NEXT %2 ; адрес следующей строки
    %else
        %error "Invalid second value"
    %endif

%endmacro
