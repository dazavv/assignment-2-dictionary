from subprocess import Popen, PIPE
import os

inputs = ["n", "first", "second", "third", "----------------bvcdsasdfghj87654wwsdcfvbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb, "a"*255, "b"*256]
outputs = ["", "thank", "you", "dear", ""]
errors = ["This entry is not in the list", "", "This entry is not in the list", "", "Line is overflow"]

if os.path.exists("./main"):
    print("Running tests....")

    for i in range(len(inputs)):
        p = Popen(["./main"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
        data = p.communicate(inputs[i].encode())
        if data[0].decode().strip() == outputs[i] and data[1].decode().strip() == errors[i]:
            print("Test " + str(i+1) + " passed")
        else:
            print("Test " + str(i+1) + ' failed. {Output: "' + data[0].decode().strip() + '", Error: "' + data[1].decode().strip() + '"} {Need output: "' + outputs[i] + '", Need error: "' + errors[i] + '"}')

else:
    print("File does not exist")
