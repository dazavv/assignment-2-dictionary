%include "lib.inc"

section .text
global find_word

; find_word принимает на вход нуль-терминированную строку (rdi) и указатель на начало словаря (rsi)
find_word:
	push r9
	push r8
	mov r9, rdi ; Сделаем копии наших входных значений (чтобы в цикле не делать лишних обращений к памяти через push/pop)
	mov r8, rsi
	.loop:
		test r8, r8 ; Проверка на пустой узел (достижение конца словаря)
		jz .not_found

		mov rdi, r9
		lea rsi, [r8 + 8]
		call string_equals
		test rax, rax
		jnz .found
		mov r9, [r9]
		jmp .loop
	.found:
		mov rax, r9
		pop r9
		pop r8
		ret
	.not_found:
		xor rax, rax
		pop r9
		pop r8
		ret	
